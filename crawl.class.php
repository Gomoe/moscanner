<?php

require "PHPCrawl_083/libs/PHPCrawler.class.php";


    class Crawler extends PHPCrawler {
    	private $error_lines = [];
    	public $flagz = false;
        private $results = [];
        public function __construct ($url){
          parent::__construct();
          $this->setURL($url);
          $this->addContentTypeReceiveRule("#text/html#");
          $this->addURLFilterRule("#\.(jpg|jpeg|gif|png)$# i");
          $this->enableCookieHandling(true);
          $this->setTrafficLimit(1000 * 1024);
      }

      public function setArray($arr) {
          $this->error_lines = $arr;
      }

      private function curl_get_contents($url)
      {
          $ch = curl_init($url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
          curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
          $data = curl_exec($ch);
          curl_close($ch);
          return $data;
      }


      function handleDocumentInfo($url){
        $this->results[] = $url;
    }

    public function getResults() {
        return $this->results;
    }
}




?>