<?php
require_once 'crawlController.class.php';
require_once 'response.class.php';
class MoeService {
	private $crawl;
	public function __construct() {
		$this->crawl = new CrawlerController ();
	}
	private function makeResponse($crawlResults) {
		$resp = new MoeResponse ();
		$resp->setUrls ( $crawlResults [urls] );
		$resp->setSqlVun ( $crawlResults [sql] );
		$resp->setReport ( $crawlResults [report] );
		// var_dump ( $resp );
		return $resp;
	}
	public function getResponse($request) {
		$crawlResults = $this->crawl->crawl ( $request );
		return $this->makeResponse ( $crawlResults );
	}
	public function getDefaultResponse() {
		$crawlResults = $this->crawl->defaultCrawl ();
		return $this->makeResponse ( $crawlResults );
	}
}
?>