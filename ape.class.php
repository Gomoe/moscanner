<?php 
require_once 'crawlController.class.php';
require_once 'api.class.php';

class Api extends Apii {
	private $kontrol;
	public function __construct($req) {
		parent::__construct($req);
		$this->kontrol = new CrawlerController();
	}

	protected function crawl() {
		if($this->method == 'POST') {
			return $this->kontrol->crawl($this->request);
		} else if ($this->method == 'GET') {
			return $this->kontrol->defaultCrawl();
		}
	}

	protected function tester() {
		if($this->method == 'GET') {
			return "Hello JAVAFX";
		}
	}

	protected function FunctionName()
	{
		# code...
	}

}


?>