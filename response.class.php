<?php
class MoeResponse implements \JsonSerializable {
	private $urls;
	private $report;
	private $sqlVun = false;
	private $xss = false;
	public function getUrls() {
		return $this->urls;
	}
	public function setUrls($urls) {
		$this->urls = $urls;
		return $this;
	}
	public function getSqlVun() {
		return $this->sqlVun;
	}
	public function setSqlVun($sqlVun) {
		$this->sqlVun = $sqlVun;
		return $this;
	}
	public function getXss() {
		return $this->xss;
	}
	public function setXss($xss) {
		$this->xss = $xss;
		return $this;
	}
	public function getReport() {
		return $this->report;
	}
	public function setReport($report) {
		$this->report = $report;
		return $this;
	}
	public function jsonSerialize() {
		return get_object_vars ( $this );
	}
}
?>