
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Vulnerability Scanner</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <style>
    .jumbotron{
      background-color: black;
      text-align: center;
      color: lightblue;
      padding-left: 50px;
    }

    .col-xs-4{
      width: 1000px;
      float: none;
    }

    body {
      background-color: black;
    }

  }
</style>
</head>
<body>
  <div class="jumbotron">
    <h1> Web Vulnerability Scanner</h1> 
    <p>Enter the target website url:</p>
    <!-- <form method="post" action="index.php"> -->
      <div class="container-1">
        <div class="col-xs-4" style="display:inline-flex;">
          <input class="form-control" id="curl" name="url" type="text" placeholder="www.Example.com ... " style="margin-right: 5px;">
          <button type="submit" class="btn btn-default" onclick="crawl();">Scan</button>
        </div>
      </div>
    </div> 

<?php 
  require_once "crawl.class.php";

  $url = $_POST['url'];
  if(isset($url)) {
    $lines = file('errors.txt');
    $crawl = new Crawler($url);
    $crawl->setArray($lines);
    $crawl->setURL($url);


    $myHtml = file_get_html($url);
    $xxx = $myHtml->find("input[type=\"text\"]",0);
    echo "<script type=\"text/javascript\">alert('OKAY test::::: ".$xxx->id."');</script>";

    $report = $crawl->getProcessReport();
    echo "<div class='jumbotron' style=''><p>Summary:"."</p>".$lb;
    echo "<p>Documents received: ".$report->files_received."</p>".$lb;
    echo "<p>Bytes received: ".$report->bytes_received." bytes</p>".$lb;
    echo "<p>Process runtime: ".$report->process_runtime." sec</p>".$lb; 

    if($crawl->flagz){

      echo "<p>Security Status: Low </p>".$lb;

    }

    echo "<p>Security Status: Normal</p></div>".$lb;
   

  }
?>

<!-- </form> -->
</div>
</div>
<script type="text/javascript" src="moe.js"></script>
</body>
</html>
