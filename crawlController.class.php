<?php
	require_once 'crawl.class.php';
class CrawlerController{

  public function crawl($url){
  	$k = json_encode($url);
    $crawl = new Crawler($k->url);
    $crawl->go();
    return $crawl->getResults();
  }

  public function defaultCrawl() {
  	$crawl = new Crawler("http://www.google.com");
    $crawl->go();
    return $crawl->getResults();
  }
}
?>